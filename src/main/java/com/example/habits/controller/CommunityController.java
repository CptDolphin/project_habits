package com.example.habits.controller;

import com.example.habits.model.dto.CommunityDto;
import com.example.habits.model.Community;
import com.example.habits.service.ICommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(path = "/community/")
public class CommunityController {

    @Autowired
    private ICommunityService communityService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CommunityDto create(@RequestBody @Valid Community Community, BindingResult bindingResult) {
        return new CommunityDto(communityService.create(Community,bindingResult));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CommunityDto update(
            @PathVariable("id") Long id,
            @RequestBody @Valid Community Community, BindingResult bindingResult) {
        return new CommunityDto(communityService.updateById(Community, id, bindingResult));
    }
  
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CommunityDto getById(@RequestParam("id")Long id) {
        return new CommunityDto(communityService.getById(id));
    }
    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam("id")Long id) { communityService.remove(id);
    }

    // todo : wybranie użytkowników należących do podanego community

//    @GetMapping
//    @ResponseStatus(HttpStatus.OK)
//    public Page<CommunityDto> search(
//            @RequestParam(value = "name",required = false, defaultValue = "")String name,
//            @RequestParam(value = "time", required = false)Double time, Pageable pageable) {
//        Page<Community> result = communityService.search(name,time,pageable);
//        return new PageImpl(
//                result.stream()
//                        .map(CommunityDto::new)
//                        .collect(Collectors.toList()),
//                pageable,
//                result.getTotalElements());
//    }
}

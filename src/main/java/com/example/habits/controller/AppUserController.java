package com.example.habits.controller;

import com.example.habits.model.dto.AppUserDto;
import com.example.habits.model.AppUser;
import com.example.habits.service.IAppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(path = "/appuser/")
public class AppUserController {

    @Autowired
    private IAppUserService appUserService;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public AppUserDto create(@RequestBody AppUser appUser, BindingResult bindingResult) {
        return new AppUserDto(appUserService.create(appUser,bindingResult));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AppUserDto update(
            @PathVariable("id") Long id,
            @RequestBody @Valid AppUser AppUser, BindingResult bindingResult) {
        return new AppUserDto(appUserService.updateById(AppUser, id, bindingResult));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AppUserDto getById(@RequestParam("id")Long id) {
        return new AppUserDto(appUserService.getById(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam("id")Long id) { appUserService.remove(id);
    }
//    @GetMapping
//    @ResponseStatus(HttpStatus.OK)
//    public Page<AppUserDto> search(
//            @RequestParam(value = "name",required = false, defaultValue = "")String name,
//            @RequestParam(value = "time", required = false)Double time, Pageable pageable) {
//        Page<AppUser> result = appUserService.search(name,time,pageable);
//        return new PageImpl(
//                result.stream()
//                        .map(AppUserDto::new)
//                        .collect(Collectors.toList()),
//                pageable,
//                result.getTotalElements());
//    }
}

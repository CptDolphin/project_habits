package com.example.habits.controller;

import com.example.habits.model.AppUser;
import com.example.habits.model.Habit;
import com.example.habits.model.Role;
import com.example.habits.model.dto.*;
import com.example.habits.response.ResponseFactory;
import com.example.habits.service.AppUserService;
import com.example.habits.service.HabitService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.habits.config.JWTFilter.ENCRYPTION_SECRET;

@RestController
@CrossOrigin
@RequestMapping(path = "/auth/")
public class AuthenticationController {
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private HabitService habitService;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody AppUserRegisterDto dto) {
        Optional<AppUserDto> user = appUserService.registerUser(dto);
        if (user.isPresent()) {
            return ResponseFactory.created(user.get());
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity authenticate(@RequestBody LoginDto dto) {
        Optional<AppUser> userOptional = appUserService.getUserWithLoginAndPassword(dto);
        if (userOptional.isPresent()) {
            AppUser user = userOptional.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            byte[] apiPassword = DatatypeConverter.parseBase64Binary(ENCRYPTION_SECRET);
            Key key = new SecretKeySpec(apiPassword, signatureAlgorithm.getJcaName());

            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setIssuedAt(new Date())
                    .claim("roles", translateRoles(user.getRoleSet()))
                    .signWith(signatureAlgorithm, key)
                    .compact();
            return ResponseFactory.ok(new AuthenticationDto(user.getLogin(), user.getId(), token));
        }
        return ResponseFactory.badRequest();
    }

    private Set<String> translateRoles(Set<Role> roles) {
        return roles
                .stream()
                .map(role -> role.getName())
                .collect(Collectors.toSet());
    }

    @RequestMapping(path = "/habit", method = RequestMethod.POST)
    public Habit create(@RequestBody Habit habit, BindingResult bindingResult) {
        return habitService.create(habit, bindingResult);
    }
}

package com.example.habits.controller;

import com.example.habits.model.dto.HabitDto;
import com.example.habits.model.Habit;
import com.example.habits.service.IHabitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(path = "/habit/")
public class HabitController {

    @Autowired
    private IHabitService habitService;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public HabitDto create(@RequestBody @Valid Habit habit, BindingResult bindingResult) {
        return new HabitDto(habitService.create(habit,bindingResult));
    }
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HabitDto update(
            @PathVariable("id") Long id,
            @RequestBody @Valid Habit habit, BindingResult bindingResult) {
        return new HabitDto(habitService.updateById(habit, id, bindingResult));
    }
//    @GetMapping
//    @ResponseStatus(HttpStatus.OK)
//    public Page<HabitDto> search(
//            @RequestParam(value = "name",required = false, defaultValue = "")String name,
//            @RequestParam(value = "time", required = false)Double time, Pageable pageable) {
//        Page<Habit> result = habitService.search(name,time,pageable);
//        return new PageImpl(
//                result.stream()
//                        .map(HabitDto::new)
//                        .collect(Collectors.toList()),
//                pageable,
//                result.getTotalElements());
//    }
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HabitDto getById(@RequestParam("id")Long id) {
        return new HabitDto(habitService.getById(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam("id")Long id) { habitService.remove(id);
    }
}
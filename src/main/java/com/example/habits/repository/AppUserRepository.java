package com.example.habits.repository;

import com.example.habits.model.AppUser;
import com.example.habits.model.Habit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    boolean existsByName(String name);
    void deleteById(Long id);
    Page<AppUser> findByNameIgnoreCase(String name, Pageable pageable);

    Optional<AppUser> getById(Long id);

    Optional<AppUser> findByLogin(String login);
}

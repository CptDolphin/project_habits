package com.example.habits.repository;

import com.example.habits.model.Habit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HabitRepository extends JpaRepository<Habit, Long> {
    boolean existsByName(String name);
    void deleteById(Long id);
    Page<Habit> findByNameIgnoreCase(String name, Pageable pageable);
}

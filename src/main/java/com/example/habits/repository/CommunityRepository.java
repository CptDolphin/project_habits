package com.example.habits.repository;

import com.example.habits.model.Community;
import com.example.habits.model.Habit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommunityRepository extends JpaRepository<Community, Long> {
    boolean existsByName(String name);
    void deleteById(Long id);
    Page<Community> findByNameIgnoreCase(String name, Pageable pageable);

    Optional<Community> getByName(String name);
}

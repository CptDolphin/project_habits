package com.example.habits.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JWTFilter extends GenericFilterBean {
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String BEARER = "Bearer ";
    public static final String ENCRYPTION_SECRET = "czosnek";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String authHeader = request.getHeader(AUTHORIZATION_HEADER);
        if (authHeader == null || !authHeader.contains(BEARER)) {
            //blad - nie zalogowany
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not logged in");
        } else {
            //jestesmy zalogowani
            String encryptedInfo = authHeader.substring(BEARER.length());
            Claims claims = Jwts.parser().setSigningKey(ENCRYPTION_SECRET).parseClaimsJws(encryptedInfo).getBody();
            request.setAttribute("claims", claims);
            SecurityContextHolder.getContext().setAuthentication(getAuthentication(claims));
            //jak juz zrobilismy
            //to przekazujemy zapytanie oraz odpowiedz dalej do lancucha filtrow

            filterChain.doFilter(request, response);
        }
    }
//@Override
//public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
//
//    HttpServletRequest request = (HttpServletRequest) req;
//    String authHeader = request.getHeader(AUTHORIZATION_HEADER);
//    if (authHeader == null || !authHeader.startsWith("Bearer ")) {
//        ((HttpServletResponse) res).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Authorization header.");
//    } else {
//        try {
//            String token = authHeader.substring(7);
//            Claims claims = Jwts.parser().setSigningKey(ENCRYPTION_SECRET).parseClaimsJws(token).getBody();
//            request.setAttribute("claims", claims);
//            SecurityContextHolder.getContext().setAuthentication(getAuthentication(claims));
//            filterChain.doFilter(req, res);
//        } catch (SignatureException e) {
//            ((HttpServletResponse) res).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid token");
//        }
//    }
//}

    //    private Authentication getAuthentication(Claims claims) {
//
//        List<SimpleGrantedAuthority> uprawnienia =
//                new ArrayList(Arrays.asList(new SimpleGrantedAuthority("USER")));
//        User user = new User(claims.getSubject(), "", uprawnienia);
//        UsernamePasswordAuthenticationToken token =
//                new UsernamePasswordAuthenticationToken(user,uprawnienia);
//        return token;
//    }
    private Authentication getAuthentication(Claims claims) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        List<String> roles = (List<String>) claims.get("roles");
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        User principal = new User(claims.getSubject(), "", authorities);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                principal, "", authorities);
        return usernamePasswordAuthenticationToken;
    }
}

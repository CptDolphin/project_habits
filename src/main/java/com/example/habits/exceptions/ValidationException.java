package com.example.habits.exceptions;

public class ValidationException extends RuntimeException {
    public ValidationException(String message) { super(message); }
}

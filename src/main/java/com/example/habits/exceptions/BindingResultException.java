package com.example.habits.exceptions;

import lombok.Data;
import org.springframework.validation.BindingResult;

@Data
public class BindingResultException extends RuntimeException {
    private final BindingResult result;

    public BindingResultException(BindingResult bindingResult) {
        this.result = bindingResult;
    }
}

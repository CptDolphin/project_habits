package com.example.habits.components;

import com.example.habits.model.Role;
import com.example.habits.model.dto.AppUserRegisterDto;
import com.example.habits.repository.RoleRepository;
import com.example.habits.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    private AppUserService appUserService;
    private RoleRepository roleRepository;

    @Autowired
    public DataInitializer(AppUserService userService, RoleRepository roleRepository) {
        this.appUserService = userService;
        this.roleRepository = roleRepository;
        initalize();
    }

    private void initalize() {
        Role adminRole = new Role("admin");
        roleRepository.save(adminRole);
        if (!appUserService.getAppUserWithId(1L).isPresent()) {
            appUserService.registerUser(new AppUserRegisterDto("admin", "admin", "admin"));
        }
    }

}

package com.example.habits.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Role {
    @Id
    // sequence - pytamy o id a nastepnie wstawiamy
    // identitty - wstawiamy a potem pytamy
    // table - tak jak sequence (w calej tabeli)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    public Role(String name){ this.name =  name; }

}

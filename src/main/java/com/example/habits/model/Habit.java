package com.example.habits.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data@Table(name = "habits")
public class Habit extends BaseEntity {

    @NotNull
    @Size(min = 1, max = 100)
    @Column(nullable = false)
    private String name;

    private double time;

    @ManyToMany()
    @JsonIgnore
    private List<AppUser> appusers;
}
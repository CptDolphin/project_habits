package com.example.habits.model.dto;

import com.example.habits.model.Community;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CommunityDto extends BaseDto{
    protected String name;


    public CommunityDto(String name) {
        this.name = name;
    }
    public CommunityDto(Community communityDto) {
        this(communityDto.getName());
    }
    public CommunityDto(AppUserDto appUserDto) {
        this(appUserDto.getName());
    }
}

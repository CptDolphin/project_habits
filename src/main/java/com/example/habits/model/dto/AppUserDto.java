package com.example.habits.model.dto;

import com.example.habits.model.AppUser;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class AppUserDto extends BaseDto{
    protected String login;
    protected String name;
    protected String password;

    public AppUserDto(String name) {
        this.name = name;

    }
    public AppUserDto(AppUserDto appUser) {
        this(appUser.getName());
    }
    public AppUserDto(AppUser appUser) {
        this(appUser.getName());
    }
    public AppUserDto(long id, String login, String password, String name) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
    }

    public static AppUserDto create(AppUser user) {
        return new AppUserDto(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                user.getName());
    }
}

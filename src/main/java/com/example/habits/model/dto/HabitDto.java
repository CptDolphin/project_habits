package com.example.habits.model.dto;

import com.example.habits.model.Habit;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HabitDto extends BaseDto{
    protected String name;
    protected double time;

    public HabitDto(String name, double time) {
        this.name = name;
        this.time = time;
    }
    public HabitDto(Habit habit) {
        this(habit.getName(),habit.getTime());
    }
    public static HabitDto create(HabitDto habitDto) {
        return new HabitDto(habitDto.getName(), habitDto.getTime());
    }
}

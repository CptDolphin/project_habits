package com.example.habits.model.dto;

import lombok.Data;

@Data
public class BaseDto {
    protected long id;
}

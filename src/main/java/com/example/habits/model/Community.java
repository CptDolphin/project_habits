package com.example.habits.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Community extends BaseEntity {

    private String name;

    @ManyToMany()
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<AppUser> members;
}

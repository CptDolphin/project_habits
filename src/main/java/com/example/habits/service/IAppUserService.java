package com.example.habits.service;

import com.example.habits.model.AppUser;
import com.example.habits.model.dto.AppUserDto;
import com.example.habits.model.dto.AppUserRegisterDto;
import com.example.habits.model.dto.LoginDto;
import org.springframework.validation.BindingResult;

import java.util.Optional;

public interface IAppUserService {
    AppUser create(AppUser appUser,BindingResult bindingResult);
    AppUser updateById(AppUser appUser, Long id,BindingResult bindingResult);
    AppUser getById(Long id);
    void remove(Long id);
    Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto);

    Optional<Object> getAppUserWithId(long l);
    Optional<AppUserDto> registerUser(AppUserRegisterDto user);
}

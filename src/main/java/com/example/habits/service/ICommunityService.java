package com.example.habits.service;

import com.example.habits.model.Community;
import org.springframework.validation.BindingResult;

public interface ICommunityService {
    Community create(Community community, BindingResult bindingResult);
    Community updateById(Community community, Long id, BindingResult bindingResult);
    Community getById(Long id);
    void remove(Long id);
}

package com.example.habits.service;

import com.example.habits.exceptions.BindingResultException;
import com.example.habits.exceptions.NotFoundException;
import com.example.habits.model.Community;
import com.example.habits.repository.CommunityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Service
@Transactional
public class CommunityService implements ICommunityService{

    @Autowired
    private CommunityRepository communityRepository;

    public Community create(@Valid Community community, BindingResult bindingResult) {
        validateCommunity(community.getName(),null,bindingResult);
        return communityRepository.save(community);
    }


    public Community updateById(@Valid Community community, Long id, BindingResult bindingResult) {
        Community existingCommunity = getById(id);
        validateCommunity(community.getName(),existingCommunity.getName(),bindingResult);
        community.setId(id);
        return communityRepository.save(community);
    }


    public Community getById(Long id) {
        return communityRepository.findById(id)
                .orElseThrow(()
                        -> new NotFoundException(String.format("Community with %s id was not found",id)));
    }

    public void remove(Long id) {
        communityRepository.deleteById(id);
    }

    private void validateCommunity(String name, String currentName, BindingResult bindingResult) {
        if (communityRepository.existsByName(name) && !name.equals(currentName)) {
            bindingResult.addError(new FieldError("Community", "name", String.format("Community with %s name already exists", name)));
        }
        if (bindingResult.hasErrors()) {
            throw new BindingResultException(bindingResult);
        }
    }
}

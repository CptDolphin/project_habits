package com.example.habits.service;

import com.example.habits.model.Habit;
import org.springframework.validation.BindingResult;

public interface IHabitService {
    Habit create(Habit habit, BindingResult bindingResult);
    Habit updateById(Habit habit, Long id, BindingResult bindingResult);
    Habit getById(Long id);
    void remove(Long id);
}

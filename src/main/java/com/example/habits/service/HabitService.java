package com.example.habits.service;

import com.example.habits.exceptions.BindingResultException;
import com.example.habits.exceptions.NotFoundException;
import com.example.habits.model.Habit;
import com.example.habits.repository.HabitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Service
@Transactional
public class HabitService implements IHabitService {

    @Autowired
    private HabitRepository habitRepository;

    public Habit create(Habit habit, BindingResult bindingResult) {
        validateHabit(habit.getName(),null,bindingResult);
        return habitRepository.save(habit);
    }


    public Habit updateById(Habit habit, Long id, BindingResult bindingResult) {
        Habit existingHabit = getById(id);
        validateHabit(habit.getName(),existingHabit.getName(),bindingResult);
        habit.setId(id);
        return habitRepository.save(habit);
    }


    public Habit getById(Long id) {
        return habitRepository.findById(id)
                .orElseThrow(()
                        -> new NotFoundException(String.format("Habit with %s id was not found",id)));
    }

    public void remove(Long id) {
        habitRepository.deleteById(id);
    }

    private void validateHabit(String name, String currentName, BindingResult bindingResult) {
        if (habitRepository.existsByName(name) && !name.equals(currentName)) {
            bindingResult.addError(new FieldError("Habit", "name", String.format("Habit with %s name already exists", name)));
        }
        if (bindingResult.hasErrors()) {
            throw new BindingResultException(bindingResult);
        }
    }
}

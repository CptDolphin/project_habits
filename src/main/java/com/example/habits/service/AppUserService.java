package com.example.habits.service;

import com.example.habits.exceptions.BindingResultException;
import com.example.habits.exceptions.NotFoundException;
import com.example.habits.exceptions.UserIsInvalidException;
import com.example.habits.exceptions.UserWithThatLoginExistsException;
import com.example.habits.model.AppUser;
import com.example.habits.model.Community;
import com.example.habits.model.Role;
import com.example.habits.model.dto.AppUserDto;
import com.example.habits.model.dto.AppUserRegisterDto;
import com.example.habits.model.dto.LoginDto;
import com.example.habits.repository.AppUserRepository;
import com.example.habits.repository.CommunityRepository;
import com.example.habits.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Optional;

@Service
@Transactional
public class AppUserService implements IAppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private CommunityRepository communityRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AppUser create(@Valid AppUser appUser, BindingResult bindingResult) {
        validateAppUser(appUser.getName(), null, bindingResult);
        Community community = new Community();
        community.setName(appUser.getName());
        communityRepository.save(community);
        return appUserRepository.save(appUser);
    }


    public AppUser updateById(@Valid AppUser appUser, Long id, BindingResult bindingResult) {
        Optional<AppUser> existingAppUser = appUserRepository.getById(id);
        Optional<Community> existingCommunity = communityRepository.getByName(appUser.getName());
        if (existingAppUser.isPresent()) {
            AppUser appUser1 = existingAppUser.get();
            validateAppUser(appUser.getName(), appUser1.getName(), bindingResult);
        }
        if (existingCommunity.isPresent()) {
            Community community1 = existingCommunity.get();
            validateCommunity(community1.getName(), community1.getName(), bindingResult);
        }
        appUser.setId(id);
        return appUserRepository.save(appUser);
    }


    public AppUser getById(Long id) {
        return appUserRepository.findById(id)
                .orElseThrow(()
                        -> new NotFoundException(String.format("AppUser with %s id was not found", id)));
    }

    public void remove(Long id) {
        appUserRepository.deleteById(id);
    }

    private void validateAppUser(String name, String currentName, BindingResult bindingResult) {
        if (appUserRepository.existsByName(name) && !name.equals(currentName)) {
            bindingResult.addError(new FieldError("AppUser", "name", String.format("AppUser with %s name already exists", name)));
        }
        if (bindingResult.hasErrors()) {
            throw new BindingResultException(bindingResult);
        }
    }

    private void validateCommunity(String name, String currentName, BindingResult bindingResult) {
        if (communityRepository.existsByName(name) && !name.equals(currentName)) {
            bindingResult.addError(new FieldError("Community", "name", String.format("Community with %s name already exists", name)));
        }
        if (bindingResult.hasErrors()) {
            throw new BindingResultException(bindingResult);
        }
    }
    private boolean isInvalidUser(AppUserRegisterDto user) {
        return user.getLogin() == null || user.getLogin().isEmpty() || user.getPassword() == null || user.getPassword().isEmpty();
    }

    public Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) {
        // z bazy danych wyciągam użytkownika o loginie
        Optional<AppUser> userFromDB = appUserRepository.findByLogin(dto.getLogin());
        if (userFromDB.isPresent()) { // jeśli taki istnieje
            AppUser user = userFromDB.get(); // sprawdzam jego hasło (niżej)
            if (bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
                return userFromDB; // jeśli hasło się zgadza, to zwracam użytkownika
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<Object> getAppUserWithId(long l) {
        return Optional.empty();
    }

    public Optional<AppUserDto> registerUser(AppUserRegisterDto user) {
        if (isInvalidUser(user)) {
            try {
                throw new UserIsInvalidException();
            } catch (UserIsInvalidException e) {
                e.printStackTrace();
            }
        }
        if (isLoginRegistered(user.getLogin())) {
            try {
                throw new UserWithThatLoginExistsException();
            } catch (UserWithThatLoginExistsException e) {
                e.printStackTrace();
            }
        }
//        HashSet<Role> set = new HashSet<>();
//        set.add(roleRepository.findById(1l).get());

        AppUser userToRegister = new AppUser(user.getLogin(), user.getPassword(), user.getName());
        userToRegister.setPassword(bCryptPasswordEncoder.encode(userToRegister.getPassword()));
        userToRegister = appUserRepository.save(userToRegister);
        return Optional.ofNullable(AppUserDto.create(userToRegister));
    }

    private boolean isLoginRegistered(String login) {
        return appUserRepository.findByLogin(login).isPresent();
    }
}
